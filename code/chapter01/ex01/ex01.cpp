// ex01.cpp
//  Write a C++ program that displays your name and address (or if you value your
//  privacy, a fictitious name and address).
#include <iostream>

int main()
{
	using namespace std;

	cout << "my name: " << "MXY" << endl;
	cout << "my address: " << "Anqing, China" << endl;

	return 0;
}
// ex02.cpp
// Write a C++ program that asks for a distance in furlongs and converts it to yards.
// (One furlong is 220 yards.)
//

#include <iostream>
 
int main()
{
	using namespace std;

	double dist_furlongs; //
	double dist_yards;
	double ratio=220;

	cout << "the distance (furlongs) is ";
	cin >> dist_furlongs;
	cout << "1 furlong equals " << ratio << " yards";
	dist_yards = dist_furlongs*ratio;
	cout << ", so the distance (yards) is " << dist_yards << endl;

	return 0;
}
// ex03.cpp
// Write a C++ program that uses three user - defined functions(counting  main() as
//	one) and produces the following output :
// Three blind mice
// Three blind mice
// See how they run
// See how they run
// One function, called two times, should produce the first two lines, and the remain -
// ing function, also called twice, should produce the remaining output.

#include <iostream>

using namespace std;

void fun_out1(void);
void fun_out2(void);


int main(void)
{
	fun_out1();
	fun_out1();

	fun_out2();
	fun_out2();

	return 0;
}

void fun_out1(void)
{
	cout << "Three blind mice" << endl;
}

void fun_out2(void)
{
	cout << "See how they run" << endl;
}
// ex04.cpp
// Write a program that asks the user to enter his or her age.The program then should
// display the age in months :
// Enter your age : 29
// Your age in months is 348.

#include<iostream>

int main(void)
{
	using namespace std;

	int age;
	int months;
	int monthsperyear = 12;

	cout << "Enter your age: ";
	cin >> age;
	months = age*monthsperyear;
	cout << "Your age in months is " << months << endl;

	return 0;
}
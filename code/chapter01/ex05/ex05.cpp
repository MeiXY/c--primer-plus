// ex05.cpp
// Write a program that has main() call a user - defined function that takes a Celsius
// temperature value as an argument and then returns the equivalent Fahrenheit value.
// The program should request the Celsius value as input from the user and display
// the result, as shown in the following code :
// Please enter a Celsius value : 20
// 20 degrees Celsius is 68 degrees Fahrenheit

#include<iostream>

void celsius2fahrenheit(float censiusVal);

int main(void)
{
	float celsiusVal;

	std::cout << "Please enter a Celsius value: ";
	std::cin >> celsiusVal;
	celsius2fahrenheit(celsiusVal);

	return 0;
}

void celsius2fahrenheit(float censiusVal)
{
	float basicRatio = 1.8;
	float basicConst = 32.0;
	float fahrenheitVal;

	fahrenheitVal = censiusVal*basicRatio + basicConst;
	std :: cout << censiusVal << " degrees Celsius is " << fahrenheitVal << " degrees Fahrenheit." << std :: endl;
}
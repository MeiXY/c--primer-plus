// Sales.cpp
#include<iostream>
#include "Sales.h"

namespace SALES
{
	// 程序中定义
	void setSales(TSales & s, const double pdbAr[], int n)
	{
		// 检查
		if (n > QUARTERS || n<0)
		{
			std::cout << "number cannot over the range(0~" << QUARTERS << ")" << std::endl;
		}
		if (pdbAr == NULL)
		{
			std::cout << "input parameter pointer cannot be NULL" << std::endl;
		}

		// 
		s.dbMax = pdbAr[0];
		s.dbMin = pdbAr[0];
		s.dbAverage = 0.0; // 初值，无-出错
		for (int i = 0; i < n; i++)
		{
			s.pdbSales[i] = pdbAr[i];
			s.dbAverage += pdbAr[i];
			s.dbMax = s.dbMax > pdbAr[i] ? s.dbMax : pdbAr[i];
			s.dbMin = s.dbMin < pdbAr[i] ? s.dbMin : pdbAr[i];
		}
		s.dbAverage = s.dbAverage / n; // 平均值
	}
	// 交互式
	void setSales(TSales & s)
	{
		s.dbAverage = 0.0; // 初值，无-出错
		std::cout << "please input sales for 4 quarters.." << std::endl;
		for (int i = 0; i < QUARTERS; i++)
		{
			std::cout << "quarters" << i + 1 << ": ";
			std::cin >> s.pdbSales[i];
			s.dbAverage += s.pdbSales[i];
			if (i == 0)
			{
				s.dbMax = s.pdbSales[i];
				s.dbMin = s.pdbSales[i];
			}
			else
			{
				s.dbMax = s.dbMax > s.pdbSales[i] ? s.dbMax : s.pdbSales[i];
				s.dbMin = s.dbMin < s.pdbSales[i] ? s.dbMin : s.pdbSales[i];
			}
			s.dbAverage = s.dbAverage / QUARTERS;	// 平均值
		}		
	}
	// 
	void showSales(const TSales & s)
	{
		std::cout << "sales list:" << std::endl;
		for (int i = 0; i < QUARTERS; i++)
		{
			std::cout << "quarter" << i + 1 << ": " << s.pdbSales[i] << std::endl;
		}
		std::cout << "average: " << s.dbAverage << std::endl;
		std::cout << "max: " << s.dbMax << std::endl;
		std::cout << "min: " << s.dbMin << std::endl;		
	}
}


// Sales.h
#pragma once
namespace SALES
{
	const int QUARTERS = 4;
	struct TSales
	{
		double pdbSales[QUARTERS];
		double dbAverage;
		double dbMax;
		double dbMin;
	};
	//
	void setSales(TSales& s, const double pdbAr[], int n);
	//
	void setSales(TSales& s);
	// 
	void showSales(const TSales& s);
}
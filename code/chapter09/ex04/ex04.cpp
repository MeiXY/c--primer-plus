// exp4.cpp
#include"Sales.h"
#include<iostream>

int main(int argc, char* argv[])
{
	SALES::TSales sal;
	double pdbAr[] = { 1,2,3,4 };
	
	// 程序中输入
	SALES::setSales(sal, pdbAr, 4);
	SALES::showSales(sal);	// 显示
	// 交互式输入
	SALES::setSales(sal);	
	SALES::showSales(sal);	// 显示

	system("pause");
	return 0;
}
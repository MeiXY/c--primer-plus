// usestock0.cpp -- the client program
// compile with stock00.cpp
#include <iostream>
#include "stock00.h"

int main(void)
{
	Stock person1;
	//person1=new Stock()
	person1.acquire("Jack", 100, 6.7);
	person1.buy(20, 3.2);
	person1.sell(30, 2.5);
	person1.show();

	person1.update(9.8);
	person1.show();

	return 0;
}
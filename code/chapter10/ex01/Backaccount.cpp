// Backaccount.cpp
#include <iostream>
#include "Bankaccount.h"

// constructor
CBankaccount::CBankaccount()
{
	m_strName = "none";
	m_strNumber = "000";
	m_dbBalance = 0;
}

CBankaccount::CBankaccount(const std::string & strNa, const std::string & strNum, double dbBa)
{
	m_strName = strNa;
	m_strNumber = strNum;
	m_dbBalance = dbBa;
}

// destructor
CBankaccount::~CBankaccount()
{
}

void CBankaccount::Display() const
{
	using std::cout;
	cout << "Bank account list:\n"
		<< " depositor's name: " << m_strName
		<< " account number: " << m_strNumber
		<< " balance: " << m_dbBalance << '\n';
}

void CBankaccount::Deposite(double dbMoney)
{
	using std::cout;
	if (dbMoney < 0)
		cout << "deposition money can't be negative!" << '\n';
	else
	{
		cout << "deposite ��" << dbMoney << '\n';
		m_dbBalance += dbMoney;
	}
		
}

void CBankaccount::Withdraw(double dbMoney)
{
	using std::cout;
	if (dbMoney < 0)
		cout << "deposition money can't be negative!" << '\n';
	else if (dbMoney > m_dbBalance)
		cout << "You can't withdraw more than you have!" << '\n';
	else
	{
		cout << "withdraw ��" << dbMoney << '\n';
		m_dbBalance -= dbMoney;
	}
	
}
#pragma once
// Bankaccount.h
#ifndef BANKACCOUNT_H_
#define BANKACCOUNT_H_
#include <string>
class CBankaccount
{
private:
	std::string m_strName;
	std::string m_strNumber;
	double m_dbBalance;

public:
	CBankaccount();
	CBankaccount(const std::string & strNa, const std::string & strNum, double dbBa = 0.0);
	~CBankaccount();

	void Display() const;
	void Deposite(double dbMoney);
	void Withdraw(double dbMoney);
};
#endif
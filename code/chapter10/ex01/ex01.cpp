// ex01.cpp
// Provide method definitions for the class described in Chapter Review Question 5
// and write a short program that illustrates all the features.
// Define a class to represent a bank account.Data members should include the
// depositor's name, the account number(use a string), and the balance.Member functions should allow the following :
//   Creating an object and initializing it.
//   Displaying the depositor's name, account number, and balance
//   Depositing an amount of money given by an argument
//   Withdrawing an amount of money given by an argument
// Just show the class declaration, not the method implementations. (Programming
//	Exercise 1 provides you with an opportunity to write the implementation.)
#include <iostream>
#include "Bankaccount.h"

int main()
{
	// initialization
	CBankaccount per1 = CBankaccount("Li", "100", 500);
	per1.Display();
	// deposite and withdraw
	per1.Deposite(1000);
	per1.Display();

	per1.Withdraw(300);
	per1.Display();

	return 0;
}
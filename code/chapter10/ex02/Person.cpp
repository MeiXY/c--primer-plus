// Person.cpp
#define  _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstring>
#include "Person.h"

CPerson::CPerson(const std::string & strLN, const char * pszFN)
{
	m_strLName = strLN;
	strncpy(m_pszFName, pszFN, LIMIT); // note
}

void CPerson::Show() const
{
	using std::cout;
	cout << "firstname lastname format: "
		<< m_pszFName << " " << m_strLName << '\n';
}

void CPerson::FormalShow() const
{
	using std::cout;
	cout << "lastname, firstname format: "
		<< m_strLName << ", " << m_pszFName << '\n';
}
#pragma once
#ifndef PERSON_H_
#define PERSON_H_
#include <string>

class CPerson
{
private:
	static const int LIMIT = 25;
	std::string m_strLName; // Person's last name
	char m_pszFName[LIMIT]; // Person's first name
public:
	CPerson() { m_strLName = "";  m_pszFName[0] = '\0'; } // #1
	CPerson(const std::string & strLN, const char * pszFN = "Heyyou"); // #2
	// the following methods display lname and fname
	void Show() const; // firstname lastname format
	void FormalShow() const; // lastname, firstname format
};
#endif
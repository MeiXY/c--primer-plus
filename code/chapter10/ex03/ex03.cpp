// ex03.cpp
//	Do Programming Exercise 1 from Chapter 9 but replace the code shown there with
//	an appropriate golf class declaration.Replace setgolf(golf &, const char*,
//	int) with a constructor with the appropriate argument for providing initial values.
//	Retain the interactive version of setgolf() but implement it by using the constructor. (For example, for the code for setgolf(), obtain the data, pass the data to
//		the constructor to create a temporary object, and assign the temporary object to the
//		invoking object, which is *this.)

//	// golf.h -- for pe9-1.cpp
//	const int Len = 40;
//	struct golf
//	{
//		char fullname[Len];
//		int handicap;
//	};
//	// non-interactive version:
//	// function sets golf structure to provided name, handicap
//	// using values passed as arguments to the function
//	void setgolf(golf & g, const char * name, int hc);
//	// interactive version:
//	// function solicits name and handicap from user
//	// and sets the members of g to the values entered
//	// returns 1 if name is entered, 0 if name is empty string
//	int setgolf(golf & g);
//	
//	// function resets handicap to new value
//	void handicap(golf & g, int hc);
//	// function displays contents of golf structure
//	void showgolf(const golf & g);
//	Note that setgolf() is overloaded.Using the first version of setgolf() would
//	look like this:
//	golf ann;
//	setgolf(ann, "Ann Birdfree", 24);
//	The function call provides the information that��s stored in the ann structure.Using
//	the second version of setgolf() would look like this:
//	golf andy;
//	setgolf(andy);

#include <iostream>
#include "golf.h"

int main()
{
	golf per1; // user input
	per1.showgolf();
	per1.sethandicap(100);
	per1.showgolf();

	golf per2 = golf("Liu", 30); // user definite
	per2.showgolf();
	per2.sethandicap(80);
	per2.showgolf();

	return 0;
}
// golf.cpp
#include <iostream>
#include <cstring>
#include "golf.h"

golf::golf()
{
	char pszTempName[LEN] = "";
	int nTempHandicap = 0;

	std::cout << "Please input the full name of the golf player: ";
	std::cin >> pszTempName;
	std::cout << "Please input the handicap of the golf player: ";
	std::cin >> nTempHandicap;

	*this = golf(pszTempName, nTempHandicap); // note
}

golf::golf(const char * pszName, int nHc)
{
	strncpy(m_pszFullName, pszName, LEN); // note
	m_nHandicap = nHc;
}

void golf::sethandicap(int nHc)
{
	m_nHandicap = nHc;
}

void golf::showgolf()
{
	std::cout << "the golf player information" << std::endl;
	std::cout << "fullname: " << m_pszFullName << std::endl;
	std::cout << "handicap: " << m_nHandicap << std::endl;
}
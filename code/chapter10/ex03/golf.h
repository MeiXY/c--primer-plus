#pragma once
// golf.h
#ifndef GOLF_H_
#define GOLF_H_
#endif // !GOLF_H_

class golf
{
private:
	static const int LEN = 40;
	char m_pszFullName[LEN];
	int m_nHandicap;

public:
	// non-interactive version:
	// function sets golf structure to provided name, handicap
	// using values passed as arguments to the function
	golf(const char * pszName, int nHc=1);
	// interactive version:
	// function solicits name and handicap from user
	// and sets the members of g to the values entered
	// returns 1 if name is entered, 0 if name is empty string
	golf();
	// function resets handicap to new value
	void sethandicap(int nHc);
	// function displays contents of golf structure
	void showgolf();
};
	
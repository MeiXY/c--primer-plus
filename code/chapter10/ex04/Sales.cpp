// Sales.cpp
#include"Sales.h"
#include<iostream>

namespace SALES
{
	CSales::CSales()
	{
		m_dbAverage = 0.0; // 初值，无-出错
		std::cout << "please input sales for 4 quarters.." << std::endl;
		for (int i = 0; i < QUARTERS; i++)
		{
			std::cout << "quarters" << i + 1 << ": ";
			std::cin >> m_pdbSales[i];
			m_dbAverage += m_pdbSales[i];
			if (i == 0)
			{
				m_dbMax = m_pdbSales[i];
				m_dbMin = m_pdbSales[i];
			}
			else
			{
				m_dbMax = m_dbMax > m_pdbSales[i] ? m_dbMax : m_pdbSales[i];
				m_dbMin = m_dbMin < m_pdbSales[i] ? m_dbMin : m_pdbSales[i];
			}
			m_dbAverage = m_dbAverage / QUARTERS;	// 平均值
		}

	}


	CSales::CSales(const double pdbAr[], int n)
	{
		// 检查
		if (n > QUARTERS || n<0)
		{
			std::cout << "number cannot over the range(0~" << QUARTERS << ")" << std::endl;
		}
		if (pdbAr == NULL)
		{
			std::cout << "input parameter pointer cannot be NULL" << std::endl;
		}

		// 
		m_dbMax = pdbAr[0];
		m_dbMin = pdbAr[0];
		m_dbAverage = 0.0; // 初值，无-出错
		for (int i = 0; i < n; i++)
		{
			m_pdbSales[i] = pdbAr[i];
			m_dbAverage += pdbAr[i];
			m_dbMax = m_dbMax > pdbAr[i] ? m_dbMax : pdbAr[i];
			m_dbMin = m_dbMin < pdbAr[i] ? m_dbMin : pdbAr[i];
		}
		m_dbAverage = m_dbAverage / n; // 平均值
	}


	void CSales::ShowSales()
	{
		std::cout << "sales list:" << std::endl;
		for (int i = 0; i < QUARTERS; i++)
		{
			std::cout << "quarter" << i + 1 << ": " << m_pdbSales[i] << std::endl;
		}
		std::cout << "average: " << m_dbAverage << std::endl;
		std::cout << "max: " << m_dbMax << std::endl;
		std::cout << "min: " << m_dbMin << std::endl;
	}
}

// Sales.h
#pragma once

namespace SALES
{
	const int QUARTERS = 4;
	class CSales
	{
	private:
		//static const int QUARTERS = 4;
		double m_pdbSales[QUARTERS];
		double m_dbAverage;
		double m_dbMax;
		double m_dbMin;

	public:
		CSales();
		CSales(const double pdbAr[], int n);
		void ShowSales();
	};
}

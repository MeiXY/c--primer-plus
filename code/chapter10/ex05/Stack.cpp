#include "Stack.h"

CStack::CStack()
{
	nTop = 0;
}

CStack::~CStack()
{
	;
}

bool CStack::IsEmpty() const
{
	return nTop <= 0;
}

bool CStack::IsFull() const
{
	return nTop >= MAX;
}

bool CStack::Push(const Item & item)
{
	bool bRet = false;
	if (nTop < MAX)
	{
		bRet = true;
		items[nTop++] = item;
	}
	
	return bRet;
}

bool CStack::Pop(Item & item)
{
	bool bRet = false;
	if (nTop > 0)
	{
		bRet = true;
		item = items[--nTop];
	}
	return bRet;
}

#pragma once

typedef struct
{
	char pszFullName[35];
	double dbPayment;
}Item;

class CStack
{
public:
	CStack();
	virtual ~CStack();

public:
	bool IsEmpty() const;
	bool IsFull() const;
	bool Push(const Item& item);
	bool Pop(Item& item);

private:
	enum{MAX=10};
	Item items[MAX];
	int nTop;
};
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include "Stack.h"

int main(void)
{
	Item item[3] = { {"abc", 10}, { "ABC", 20 }, { "123", 30 } };
	double dbAmount = 0;
	CStack stack;
	bool bRet = false;
	for (int i = 0; i < 3; i++)
	{
		bRet = stack.Push(item[i]);
		if (bRet)
		{
			dbAmount += item[i].dbPayment;
			printf("stack push item[name: %s, payment: %lf], the all amount is %lf.\n", item[i].pszFullName, item[i].dbPayment, dbAmount);
		}
		else
		{
			printf("stack is full, the all amount is %lf.\n", dbAmount);
		}
	}

	for (int i = 0; i < 3; i++)
	{
		Item itemTemp;
		bRet = stack.Pop(itemTemp);
		if (bRet)
		{
			dbAmount -= itemTemp.dbPayment;
			printf("stack pop item[name: %s, payment: %lf], the all amount is %lf.\n", itemTemp.pszFullName, itemTemp.dbPayment, dbAmount);
		}
		else
		{
			printf("stack is empty, the all amount is %lf.\n", dbAmount);
		}
	}

	system("pause");
	return 0;
}
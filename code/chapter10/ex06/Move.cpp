// Move.cpp
#include"Move.h"
#include<iostream>

CMove::CMove(double dbA, double dbB)
{
	if (dbA < 0.0 || dbB < 0.0)
	{
		std::cout << "input parameters are not right.." << std::endl;
		exit(1);
	}
	m_dbX = dbA;
	m_dbY = dbB;
}

void CMove::ShowMove() const
{
	std::cout << "(x, y)=" << "(" << m_dbX << ", " << m_dbY << ")" << std::endl;
}

CMove CMove::Add(const CMove & m) const
{
	CMove moveRet;
	moveRet.m_dbX = m_dbX + m.m_dbX;
	moveRet.m_dbY = m_dbY + m.m_dbY;

	return moveRet;
}

void CMove::Reset(double dbA, double dbB)
{
	m_dbX = 0.0;
	m_dbY = 0.0;
}

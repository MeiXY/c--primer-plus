// Move.h
#pragma once

class CMove
{
private:
	double m_dbX;
	double m_dbY;
	
public:
	CMove(double dbA = 0, double dbB = 0);
	void ShowMove() const;
	CMove Add(const CMove& m) const;
	void Reset(double dbA = 0, double dbB = 0);
};
// ex06.cpp
#include<iostream>
#include"Move.h"

int main(int argc, char* argv[])
{
	CMove moveA(1, 2);
	CMove moveAdd(2, 4);

	moveA.ShowMove();
	moveA=moveA.Add(moveAdd);
	moveA.ShowMove();
	moveA.Reset();
	moveA.ShowMove();

	system("pause");
	return 0;
}
#include "Plorg.h"
#include <string.h>
#include <assert.h>
#include <iostream>

CPlorg::CPlorg()
{
	strncpy(m_pszName, "Plorg", NAME_LEN);
	m_nContentIndex = 50;
}

CPlorg::CPlorg(char * pszName, int nIndex)
{
	assert(pszName != NULL);
	strncpy(m_pszName, pszName, NAME_LEN);
	m_nContentIndex = nIndex;
}

CPlorg::~CPlorg()
{
	NULL;
}

void CPlorg::SetContentIndex(int nIndex)
{
	m_nContentIndex = nIndex;
}

void CPlorg::ReportBaseInfo() const
{
	std::cout << "Plorg name: " << m_pszName << std::endl;
	std::cout << "Plorg index: " << m_nContentIndex << std::endl;
}

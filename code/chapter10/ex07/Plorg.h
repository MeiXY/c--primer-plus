#pragma once

class CPlorg 
{
public:
	CPlorg();
	CPlorg(char* pszName, int nIndex);
	virtual ~CPlorg();

public:
	enum { NAME_LEN = 20 };
	void SetContentIndex(int nIndex);
	void ReportBaseInfo() const;

private:
	char m_pszName[NAME_LEN];
	int m_nContentIndex;
};
#include <iostream>
#include "Plorg.h"


int main(void)
{
	std::cout << "default plorg" << std::endl;
	CPlorg plorgDefault;
	std::cout << "show info" << std::endl;
	plorgDefault.ReportBaseInfo();

	std::cout << "modify index = 100" << std::endl;
	plorgDefault.SetContentIndex(100);
	std::cout << "show info" << std::endl;
	plorgDefault.ReportBaseInfo();

	std::cout << "plorg (test, 100)" << std::endl;
	CPlorg plorg("test", 100);
	std::cout << "show info" << std::endl;
	plorg.ReportBaseInfo();

	std::cout << "modify index = 100" << std::endl;
	plorg.SetContentIndex(100);
	std::cout << "show info" << std::endl;
	plorg.ReportBaseInfo();

	system("pause");

	return 0;
}

#include "list.h"
#include<iostream>

int main(void)
{
	std::cout << "default construct list" << std::endl;
	CList listA;
	listA.ShowList();

	std::cout << "construct new list" << std::endl;
	ITEM pItem[3] = { 1, 2, 3 };
	CList listB(pItem, 3);
	listB.ShowList();

	std::cout << "listA is empty?" << std::endl;
	if (listA.IsEmpty())
	{
		std::cout << "yes." << std::endl;
	}
	else
	{
		std::cout << "no." << std::endl;
	}

	std::cout << "listB is full?" << std::endl;
	if (listB.IsFull())
	{
		std::cout << "yes." << std::endl;
	}
	else
	{
		std::cout << "no." << std::endl;
	}

	std::cout << "listA add item" << std::endl;
	ITEM item = 1;
	listA.Add(item);
	listA.ShowList();

	std::cout << "listB visit all items" << std::endl;
	listB.Visit(AddOne);
	listB.ShowList();

	system("pause");

	return 0;
}
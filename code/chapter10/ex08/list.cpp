#include "list.h"
#include<iostream>

CList::CList() : m_nTop (0)
{
	NULL;
}

CList::CList(const ITEM * pItems, int nCount)
{
	if (nCount <= MAX_LIST_NUM)
	{
		m_nTop = nCount;
	}
	else
	{
		std::cout << "input items is too long." << std::endl;
	}

	for (int i = 0; i < nCount; i++)
	{
		m_items[i] = pItems[i];
	}
}

CList::~CList()
{
	NULL;
}

bool CList::Add(ITEM & item) 
{
	bool bRet = true;
	if (m_nTop < MAX_LIST_NUM)
	{
		m_items[m_nTop++] = item;
	}
	else
	{
		bRet = false;
	}
	return bRet;
}

bool CList::IsEmpty() const
{
	bool bRet = false;
	if (m_nTop <= 0)
	{
		bRet = true;
	}
	return bRet;
}

bool CList::IsFull() const
{
	bool bRet = false;
	if (m_nTop >= MAX_LIST_NUM)
	{
		bRet = true;
	}
	return bRet;
}

void CList::Visit(void(*pf)(ITEM &))
{
	for (int i = 0; i < m_nTop; i++)
	{
		pf(m_items[i]);
	}
}

void CList::ShowList() const
{
	for (int i = 0; i < m_nTop; i++)
	{
		std::cout << m_items[i] << std::endl;
	}
}

void AddOne(ITEM & item)
{
	item += 1;
}

#pragma once

typedef int ITEM;

class CList
{
public:
	CList();
	CList(const ITEM* pItems, int nCount);
	virtual ~CList();

public:
	enum{MAX_LIST_NUM = 20};
	bool Add(ITEM& item);
	bool IsEmpty() const;
	bool IsFull() const;
	void Visit(void(*pf)(ITEM &));
	void ShowList() const;

private:
	ITEM m_items[MAX_LIST_NUM];
	int m_nTop;
};

void AddOne(ITEM & item);
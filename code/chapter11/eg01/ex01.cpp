// ex01.cpp
#include <iostream>
#include "mytime0.h"

int main()
{
	Time time0, time1, time2;
	time0 = Time(11, 60);
	time1 = Time(20, 30);

	time0.AddMin(30);
	time0.AddHr(5);
	time0.Show();

	time0.Reset(12, 0);
	time0.Show();

	time2 = time0.Sum(time1);
	time2.Show();

	return 0;
}
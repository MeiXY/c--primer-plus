// ex04.cpp
#include <iostream>
#include "mytime1.h"

int main()
{
	Time time0, time1, time2;
	time0 = Time(11, 30);
	time1 = Time(20, 30);

	time0.Show();
	time1.Show();

	time2 = time0.operator+(time1);
	time2.Show();

	time2 = time0 + time1;
	time2.Show();

	return 0;
}
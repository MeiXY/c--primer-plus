// mytime0.cpp
#include <iostream>
#include <cstdio>
#include "mytime1.h"

// constructor
Time::Time()
{
	hours = 0;
	minutes = 0;
}

Time::Time(int h, int m)
{
	if (m < 0 || m>59 || h < 0 || h>23)
	{
		std::cout << "the time is not right(m<0 or m>59 or h<0 or h>23) " << std::endl;
		exit(1); // error, exit
	}		
	else
	{
		hours = h;
		minutes = m;
	}
}

void Time::AddMin(int m)
{
	if (m < 0 || m>59)
	{
		std::cout << "the minutes is not right(m<0 or m>59)" << std::endl;
		exit(1);
	}		
	else
	{
		minutes += m;
		if (minutes >= 60)
		{
			minutes %= 60;
			hours += minutes / 60;
			hours %= 24;
		}
	}	
}

void Time::AddHr(int h)
{
	if (h < 0 || h>23)
	{
		std::cout << "the hours is not right(h<0 or h>23)" << std::endl;
		exit(1);
	}		
	else
	{
		hours += h;
		if (hours > 23)
			hours %= 24;
	}	
}

void Time::Reset(int h, int m)
{
	hours = h;
	minutes = m;
}

Time Time::operator+(const Time & t) const
{
	Time sum;
	if (t.minutes < 0 || t.minutes>59 || t.hours < 0 || t.hours>23)
	{
		std::cout << "the time is not right(m<0 or m>59 or h<0 or h>23) " << std::endl;
		exit(1);
	}		
	else
	{
		sum.minutes = this->minutes + t.minutes;
		if (sum.minutes >= 60)   // minutes
		{
			sum.minutes %= 60;
			sum.hours += 1;
		}
		sum.hours = sum.hours + this->hours + t.hours;
		if (sum.hours >= 24)   // hours
			sum.hours %= 24;			
	}
	return sum;
}

void Time::Show() const
{
	std::cout << "Time: " << hours << ':' << minutes << '.' << std::endl;
}

// usetime3.cpp -- using the fourth draft of the Time class
// compile usetime3.cpp and mytime3.cpp together
#include <iostream>
#include "mytime3.h"

int main()
{
	using std::cout;
	using std::endl;

	Time t1(1, 25);
	Time t2(3, 30);
	Time t3;

	cout << "time1: " << t1 << endl;;
	cout << "time2: " << t2 << endl;
	t3 = t1 + t2;   // operator+()
	cout << "time1+time2: " << t3 << endl;
	t3 = t2 - t1;   // operator-()
	cout << "time2-time1: " << t3 << endl;
	t3 = t1*0.5;   // operator*()
	cout << "time1*0.5: " << t3 << endl;
	t3 = 0.5*t1;
	cout << "0.5*time1: " << t3 << endl;

	return 0;
}
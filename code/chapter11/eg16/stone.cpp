// stone.cpp  --  user-defined conversions
// compile with stonewt.cpp
#include <iostream>
#include "stonewt.h"
using std::cout;

void display(const Stonewt & st, int n);
int main()
{
	Stonewt per1 = 100;
	Stonewt per2(101.2);
	Stonewt per3(80, 0.5);

	cout << "per1 weighted: ";
	per1.show_stn();
	cout << "per2 weighted: ";
	per2.show_stn();
	cout << "per3 weighted: ";
	per3.show_lbs();

	per2 = 105.6;
	per3 = 108.3;
	cout << "after lunch, per2 weighted: ";
	per2.show_stn();
	cout << "after lunch, per3 weighted: ";
	per3.show_stn();
	display(per3, 2);
	display(300, 3);

	return 0;
}
void display(const Stonewt & st, int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << "Wow! ";
		st.show_stn();
	}
}
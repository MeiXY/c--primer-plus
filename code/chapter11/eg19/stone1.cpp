// stone1.cpp  --  user defined conversion functions
// compile with stonewt1.cpp
#include <iostream>
#include "stonewt1.h"

int main()
{
	using std::cout;
	using std::endl;

	Stonewt per1(80, 0.5);
	// implicit conversion
	double res1 = per1;
	int res2 = per1;
	
	cout << "per1 weighted: " << res1 << endl;
	cout << "per1 weighted: " << res2 << endl;
	// explicit conversion
	cout << "per1 weighted: " << double(per1) << endl;
	cout << "per1 weighted: " << int(per1) << endl;

	return 0;
}
// ex03.cpp
// Modify Listing 11.15 so that instead of reporting the results of a single trial for a
// particular target/step combination, it reports the highest, lowest, and average number
// of steps for N trials, where N is an integer entered by the user.


// compile with the vect.cpp file
#include <iostream>
#include <cstdlib> // rand(), srand() prototypes
#include <ctime> // time() prototype
#include "vect.h"
int main()
{
	using namespace std;
	using VECTOR::Vector;
	srand(time(0)); // seed random-number generator
	double direction = 0.0;
	Vector step;
	Vector result(0.0, 0.0);
	unsigned long steps = 0;
	unsigned long uiLowestSteps = 0;
	unsigned long uiHighestSteps = 0;
	unsigned long uiAveSteps = 0;
	unsigned int uiTestCnt = 0;
	unsigned int uiTestTemp = 0;
	double target = 0.0;
	double dstep = 0.0;
	cout << "Enter target distance (q to quit): ";
	while (cin >> target)
	{
		cout << "Enter step length: ";
		if (!(cin >> dstep))
		{
			break;
		}

		cout << "Enter test count: ";
		if (!(cin >> uiTestCnt))
		{
			break;
		}

		uiLowestSteps = 0;
		uiHighestSteps = 0;
		uiAveSteps = 0;
		uiTestTemp = uiTestCnt;
		while(uiTestTemp)
		{
			while (result.magval() < target)
			{
				direction = rand() % 360;
				step.reset(dstep, direction, Vector::POL);
				result = result + step;
				steps++;
			}
			cout << "After " << steps << " steps, the subject "
				"has the following location:\n";
			cout << result << endl;
			result.polar_mode();
			cout << " or\n" << result << endl;
			cout << "Average outward distance per step = "
				<< result.magval() / steps << endl;

			if (0 == uiLowestSteps || steps < uiLowestSteps)
			{
				uiLowestSteps = steps;
			}

			if (steps > uiHighestSteps)
			{
				uiHighestSteps = steps;
			}

			uiAveSteps += steps;

			steps = 0;
			result.reset(0.0, 0.0);

			uiTestTemp--;
		}

		cout << "After " << uiTestCnt << " trails, the lowest steps is " << uiLowestSteps << endl;
		cout << "the highest steps is " << uiHighestSteps << endl;
		cout << "the average steps is " << uiAveSteps / uiTestCnt << endl;
		cout << "Enter target distance (q to quit): ";
	}
	cout << "Bye!\n";
	cin.clear();
	while (cin.get() != '\n')
		continue;
	return 0;
}


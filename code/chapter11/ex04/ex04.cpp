// ex04.cpp
// Rewrite the final Time class example(Listings 11.10, 11.11, and 11.12) so that all
// the overloaded operators are implemented using friend functions.
#include <iostream>
#include "mytime.h"

int main()
{
	using std::cout;
	using std::endl;

	CTime t1(3, 30), t2(5, 40), t3;

	cout << "add=>";
	t3 = t1 + t2;
	cout << t3 << endl;

	cout << "minus=>";
	t3 = t2 - t1;
	cout << t3 << endl;

	cout << "multiply=>";
	t3 = 0.5*t1;
	cout << t3 << endl;
	t3 = t2*0.5;
	cout << t3 << endl;

	system("pause");
	return 0;
}
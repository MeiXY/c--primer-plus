// mytime.cpp  --  implementing Time methods
#include "mytime.h"

CTime::CTime()
{
	m_nHours = m_nMinutes = 0;
}

CTime::CTime(int nH, int nM)
{
	m_nHours = nH;
	m_nMinutes = nM;
}

void CTime::AddMin(int nM)
{
	m_nMinutes += nM;
	m_nHours += m_nMinutes / 60;
	m_nMinutes %= 60;
}

void CTime::AddHr(int nH)
{
	m_nHours += nH;
}

void CTime::Reset(int nH, int nM)
{
	m_nHours = nH;
	m_nMinutes = nM;
}

CTime operator+(const CTime & tA, const CTime & tB)
{
	CTime tSum;
	tSum.m_nMinutes = tA.m_nMinutes + tB.m_nMinutes;
	tSum.m_nHours = tA.m_nHours + tB.m_nHours + tSum.m_nHours / 60;
	tSum.m_nMinutes %= 60;

	return tSum;
}

CTime operator-(const CTime & tA, const CTime & tB)
{
	CTime tDiff;
	int nTot1, nTot2;

	nTot1 = tA.m_nMinutes + 60 * tA.m_nHours;
	nTot2 = tB.m_nMinutes + 60 * tB.m_nHours;
	tDiff.m_nMinutes = (nTot1 - nTot2) % 60;
	tDiff.m_nHours = (nTot1 - nTot2) / 60;
	return tDiff;
}

CTime operator*(double dbMult, const CTime & tA)
{
	CTime tResult;
	long nTotalminutes = tA.m_nHours*dbMult * 60 + tA.m_nMinutes*dbMult;

	tResult.m_nHours = nTotalminutes / 60;
	tResult.m_nMinutes = nTotalminutes % 60;
	return tResult;
}

std::ostream & operator<<(std::ostream & os, const CTime & tA)
{
	os << tA.m_nHours << " hours, " << tA.m_nMinutes << " minutes";
	return os;
}
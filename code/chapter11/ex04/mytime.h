// mytime.h -- Time class with friends
#ifndef MYTIME_H_
#define MYTIME_H_
#include <iostream>

class CTime
{
private:
	int m_nHours;
	int m_nMinutes;
public:
	CTime();
	CTime(int nH, int nM = 0);

	void AddMin(int nM);
	void AddHr(int nH);
	void Reset(int nH = 0, int nM = 0);

	friend CTime operator+(const CTime & t, const CTime & m);
	friend CTime operator-(const CTime & t, const CTime & m);
	friend CTime operator*(double m, const CTime & t);
	friend CTime operator*(const CTime & t, double m) {return m*t; }
	friend std::ostream & operator<<(std::ostream & os, const CTime & t);
};
#endif
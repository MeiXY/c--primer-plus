// StoneWt.cpp
#include "StoneWt.h"

CStoneWt::CStoneWt()
{
	m_nState = 0;
	m_nStone = 0;
	m_dbPdsLeft = 0.0;
	m_dbPounds = 0.0;
}

CStoneWt::CStoneWt(int nSt, double dbLeft)
{
	if (nSt < 0 || dbLeft < 0)
	{
		std::cout << "paramter cannot be minus.." << std::endl;
		exit(1);
	}
	else if (dbLeft > PDSPERSTN)
	{
		std::cout << "parameter over.." << std::endl;
		exit(1);
	}

	m_nState = 0;
	m_nStone = nSt;
	m_dbPdsLeft = dbLeft;
	m_dbPounds = nSt*PDSPERSTN + dbLeft;
}

CStoneWt::CStoneWt(double dbPds)
{
	if (dbPds < 0)
	{
		std::cout << "paramter cannot be minus.." << std::endl;
		exit(1);
	}
	m_nState = 1;
	m_nStone = int(dbPds / PDSPERSTN);
	m_dbPdsLeft = dbPds - m_nStone*PDSPERSTN;
	m_dbPounds = dbPds;
}

void CStoneWt::SetState(int nState)
{
	m_nState = nState;
}

CStoneWt operator+(const CStoneWt & stoneA, const CStoneWt & stoneB)
{
	CStoneWt stoneRet;
	double dbTmpPds = stoneA.m_dbPdsLeft + stoneB.m_dbPdsLeft;
	stoneRet.m_nStone = stoneA.m_nStone + stoneB.m_nStone + int(dbTmpPds / PDSPERSTN);
	stoneRet.m_dbPdsLeft = dbTmpPds - int(dbTmpPds / PDSPERSTN)*PDSPERSTN;
	stoneRet.m_dbPounds = stoneA.m_dbPounds + stoneB.m_dbPounds;

	return stoneRet;
}

CStoneWt operator-(const CStoneWt & stoneA, const CStoneWt & stoneB)
{
	CStoneWt stoneRet;
	stoneRet.m_dbPounds = stoneA.m_dbPounds - stoneB.m_dbPounds;
	stoneRet.m_nStone = int(stoneRet.m_dbPounds / PDSPERSTN);
	stoneRet.m_dbPdsLeft = stoneRet.m_dbPounds - stoneRet.m_nStone*PDSPERSTN;

	return stoneRet;
}

CStoneWt operator*(const double & dbMulElem, const CStoneWt & stoneA)
{
	CStoneWt stoneRet;
	stoneRet.m_dbPounds = dbMulElem*stoneA.m_dbPounds;
	stoneRet.m_nStone = int(stoneRet.m_dbPounds / PDSPERSTN);
	stoneRet.m_dbPdsLeft = stoneRet.m_dbPounds - stoneRet.m_nStone*PDSPERSTN;

	return stoneRet;
}

std::ostream & operator<<(std::ostream&  os, const CStoneWt & stoneA)
{
	// TODO: insert return statement here
	if (stoneA.m_nState == STONEPS)
	{
		os << stoneA.m_nStone << " stone, " << stoneA.m_dbPdsLeft<<" pounds left\n";
	}
	else
	{
		os << stoneA.m_dbPounds << " pounds.\n";
	}
	
	return os;
}

// StoneWt.h
#include<iostream>

enum IStoneState { STONEPS, FLOATPS };
enum { PDSPERSTN = 14 };

class CStoneWt 
{
private:	
	
	int m_nState;
	int m_nStone;
	double m_dbPdsLeft;
	double m_dbPounds;

public:
	CStoneWt();
	CStoneWt(int nSt, double dbLeft);
	CStoneWt(double dbPds);
	void SetState(int nState);

	friend CStoneWt operator+(const CStoneWt& stoneA, const CStoneWt& stoneB);
	friend CStoneWt operator-(const CStoneWt& stoneA, const CStoneWt& stoneB);
	friend CStoneWt operator*(const double& dbMulElem, const CStoneWt& stoneA);
	friend CStoneWt operator*(const CStoneWt& stoneA, const double& dbMulElem) { return dbMulElem*stoneA; }
	friend std::ostream& operator<<(std::ostream& os, const CStoneWt& stoneA);
};
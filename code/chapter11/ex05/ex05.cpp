// ex05.cpp
#include<iostream>
#include"StoneWt.h"

int main(int argc, char* argv[])
{
	CStoneWt stoneA(100,12);
	CStoneWt stoneB(200);
	std::cout << "stone A:" << std::endl;
	stoneA.SetState(STONEPS);
	std::cout << stoneA;
	stoneA.SetState(FLOATPS);
	std::cout << stoneA;
	//-------------------------------------------
	std::cout << "stone B:" << std::endl;
	stoneB.SetState(STONEPS);
	std::cout << stoneB;
	stoneB.SetState(FLOATPS);
	std::cout << stoneB;
	//--------------------------------------------
	CStoneWt stoneC;
	stoneC = stoneA + stoneB;
	std::cout << "stoneA+stoneB:" << std::endl;
	stoneC.SetState(STONEPS);
	std::cout << stoneC;
	stoneC.SetState(FLOATPS);
	std::cout << stoneC;
	//--------------------------------------------
	CStoneWt stoneD;
	stoneD = stoneA - stoneB;
	std::cout << "stoneA-stoneB:" << std::endl;
	stoneD.SetState(STONEPS);
	std::cout << stoneD;
	stoneD.SetState(FLOATPS);
	std::cout << stoneD;
	//--------------------------------------------
	CStoneWt stoneE;
	stoneE = 2.5* stoneB;
	std::cout << "2.5*stoneB:" << std::endl;
	stoneE.SetState(STONEPS);
	std::cout << stoneE;
	stoneE.SetState(FLOATPS);
	std::cout << stoneE;
	//--------------------------------------------
	CStoneWt stoneF;
	stoneF = stoneB*2.5;
	std::cout << "stoneB*2.5:" << std::endl;
	stoneF.SetState(STONEPS);
	std::cout << stoneF;
	stoneF.SetState(FLOATPS);
	std::cout << stoneF;

	system("pause");
	return 0;
}
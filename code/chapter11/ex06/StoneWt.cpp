// StoneWt.cpp
#include "StoneWt.h"

CStoneWt::CStoneWt()
{
	m_nStone = 0;
	m_dbPdsLeft = 0.0;
	m_dbPounds = 0.0;
}

CStoneWt::CStoneWt(int nSt, double dbLeft)
{
	if (nSt < 0 || dbLeft < 0)
	{
		std::cout << "paramter cannot be minus.." << std::endl;
		exit(1);
	}
	else if (dbLeft > PDSPERSTN)
	{
		std::cout << "parameter over.." << std::endl;
		exit(1);
	}
	
	m_nStone = nSt;
	m_dbPdsLeft = dbLeft;
	m_dbPounds = nSt*PDSPERSTN + dbLeft;
}

CStoneWt::CStoneWt(double dbPds)
{
	if (dbPds < 0)
	{
		std::cout << "paramter cannot be minus.." << std::endl;
		exit(1);
	}

	m_nStone = int(dbPds / PDSPERSTN);
	m_dbPdsLeft = dbPds - m_nStone*PDSPERSTN;
	m_dbPounds = dbPds;
}

CStoneWt::CStoneWt(CStoneWt & stoneA)
{
	m_nStone = stoneA.m_nStone;
	m_dbPdsLeft = stoneA.m_dbPdsLeft;
	m_dbPounds = stoneA.m_dbPounds;
}

CStoneWt::~CStoneWt()
{
	;
}


bool CStoneWt::operator>(const CStoneWt & stoneA)
{
	if (m_dbPounds > stoneA.m_dbPounds)
	{
		return true;
	}
	
	return false;
}


bool CStoneWt::operator>=(const CStoneWt & stoneA)
{
	if (m_dbPounds >= stoneA.m_dbPounds)
	{
		return true;
	}

	return false;
}


bool CStoneWt::operator<(const CStoneWt & stoneA)
{
	if (m_dbPounds < stoneA.m_dbPounds)
	{
		return true;
	}

	return false;
}


bool CStoneWt::operator<=(const CStoneWt & stoneA)
{
	if (m_dbPounds <= stoneA.m_dbPounds)
	{
		return true;
	}

	return false;
}


bool CStoneWt::operator==(const CStoneWt& stoneA)
{
	if (m_dbPounds == stoneA.m_dbPounds)
	{
		return true;
	}

	return false;
}


bool CStoneWt::operator!=(const CStoneWt & stoneA)
{
	if (m_dbPounds != stoneA.m_dbPounds)
	{
		return true;
	}

	return false;
}


std::istream & operator >> (std::istream & is, CStoneWt & stoneA)
{
	// TODO: insert return statement here
	is >> stoneA.m_dbPounds;
	stoneA = CStoneWt(stoneA.m_dbPounds);

	return is;
}

std::ostream & operator<<(std::ostream&  os, const CStoneWt & stoneA)
{
	// TODO: insert return statement here	
	os << stoneA.m_nStone << " stone, " << stoneA.m_dbPdsLeft << " pounds left\n";	
	os << stoneA.m_dbPounds << " pounds.\n";	

	return os;
}

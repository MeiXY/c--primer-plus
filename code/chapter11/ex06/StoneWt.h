// StoneWt.h
#include<iostream>
enum { PDSPERSTN = 14 };
class CStoneWt
{
private:	
	int m_nStone;
	double m_dbPdsLeft;
	double m_dbPounds;

public:
	CStoneWt();
	CStoneWt(int nSt, double dbLeft);
	CStoneWt(double dbPds);
	CStoneWt(CStoneWt& stoneA);
	~CStoneWt();

	bool operator>(const CStoneWt& stoneA);
	bool operator>=(const CStoneWt& stoneA);
	bool operator<(const CStoneWt& stoneA);
	bool operator<=(const CStoneWt& stoneA);
	bool operator==(const CStoneWt& stoneA);
	bool operator!=(const CStoneWt& stoneA);
	friend std::istream& operator >> (std::istream& is, CStoneWt& stoneA);
	friend std::ostream& operator<<(std::ostream& os, const CStoneWt& stoneA);
};

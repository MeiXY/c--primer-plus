#include "complex0.h"

complex::complex()
{
	m_dbReal = 0.0;
	m_dbImag = 0.0;
}

complex::complex(const double & dbReal, const double & dbImag)
{
	m_dbReal = dbReal;
	m_dbImag = dbImag;
}

complex::complex(const complex & cpNum)
{
	m_dbReal = cpNum.m_dbReal;
	m_dbImag = cpNum.m_dbImag;
}

complex::~complex()
{
	NULL;
}

complex complex::operator+(const complex & cp) const
{
	double dbReal = m_dbReal + cp.m_dbReal;
	double dbImag = m_dbImag + cp.m_dbImag;
	return complex(dbReal, dbImag);
}

complex complex::operator-(const complex & cp) const
{
	double dbReal = m_dbReal - cp.m_dbReal;
	double dbImag = m_dbImag - cp.m_dbImag;
	return complex(dbReal, dbImag);
}

double complex::operator*(const complex & cp) const
{
	return m_dbReal*cp.m_dbReal + m_dbImag*cp.m_dbImag;
}

complex complex::operator*(const double & db) const
{
	double dbReal = db*m_dbReal;
	double dbImag = db*m_dbImag;
	return complex(dbReal, dbImag);
}

complex complex::operator~() const
{
	double dbReal = -m_dbReal;
	double dbImag = -m_dbImag;
	return complex(dbReal, dbImag);
}

complex operator*(const double & db, const complex & cp)
{
	return cp*db;
}

std::istream& operator >> (std::istream & is, complex & cp)
{
	is >> cp.m_dbReal >> cp.m_dbImag;

	return is;
}

std::ostream & operator<<(std::ostream & os, const complex & cp)
{
	// TODO: insert return statement here
	os << "(real, imag) = " << "(" << cp.m_dbReal << ", " << cp.m_dbImag << ")";

	return os;
}

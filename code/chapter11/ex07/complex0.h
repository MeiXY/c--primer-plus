#ifndef _COMPLEX0_H
#define _COMPLEX0_H

#include<iostream>

class complex 
{
public:
	// constructor
	complex();
	complex(const double& dbReal,const double& dbImag);
	complex(const complex& cpNum);
	// deconstructor
	virtual ~complex();

	// real part and imaginary part value
	inline void GetRealVal(double& dbReal) 
	{
		dbReal = m_dbReal;
	}

	inline void GetImagVal(double& dbImag) 
	{
		dbImag = m_dbImag;
	}

	complex operator+(const complex& cp) const;
	complex operator-(const complex& cp) const;
	double operator*(const complex& cp) const;
	complex operator*(const double& db) const;
	complex operator~() const;

	friend complex operator*(const double& db, const complex& cp);
	friend std::istream& operator>>(std::istream& is, complex& cp);
	friend std::ostream& operator<<(std::ostream& os, const complex& cp);
private:
	double m_dbReal;
	double m_dbImag;
};


#endif

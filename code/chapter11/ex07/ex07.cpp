// ex07.cpp
// A complex number has two parts : a real part and an imaginary part.One way to
// write an imaginary number is this : (3.0, 4.0).Here 3.0 is the real part and 4.0 is
// the imaginary part.Suppose a = (A, Bi) and c = (C, Di).Here are some complex
// operations :
//  Addition : a + c = (A + C, (B + D)i)
//  Subtraction : a - c = (A - C, (B - D)i)
//  Multiplication : a �� c = (A �� C - B��D, (A��D + B��C)i)
//  Multiplication : (x a real number) : x �� c = (x��C, x��Di)
//  Conjugation : ~a = (A, -Bi)
// Define a complex class so that the following program can use it with correct
// results :

#include <iostream>
using namespace std;
#include "complex0.h" // to avoid confusion with complex.h
int main()
{
	complex a(3.0, 4.0); // initialize to (3,4i)
	complex c;
	cout << "Enter a complex number (q to quit):\n";
	while (cin >> c)
	{
		cout << "c is " << c << '\n';
		cout << "complex conjugate is " << ~c << '\n';
		cout << "a is " << a << '\n';
		cout << "a + c is " << a + c << '\n';
		cout << "a - c is " << a - c << '\n';
		cout << "a * c is " << a * c << '\n';
		cout << "2 * c is " << 2 * c << '\n';
		cout << "Enter a complex number (q to quit):\n";
	}
	cout << "Done!\n";
	return 0;
}

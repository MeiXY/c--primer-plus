// stringbad.cpp  --  StringBad class methods
#define _CRT_SECURE_NO_WARNINGS
#include <cstring>			// string.h for some
#include "stringbad.h"
using std::cout;

// initializing static class member
int StringBad::num_strings = 0;

// class methods
// construct StringBad from C string
StringBad::StringBad(const char * s)
{
	len = std::strlen(s);				// set size
	str = new char[len + 1];		// allot storage
	std::strcpy(str, s);				// initialize pointer
	num_strings++;				// set object count
	// for your information
	cout << num_strings << ": \"" << str << "\" object created\n";
}

// default constructor
StringBad::StringBad()
{
	len = 4;
	str = new char[len];
	std::strcpy(str, "C++");		// default string
	num_strings++;
	cout << num_strings << ": \"" << str << "\" default object created\n";   // FYI
}

// copy constructor
StringBad::StringBad(const StringBad & st)
{
	num_strings++;						// handle static member update
	len = st.len;
	str = new char[len + 1];
	std::strcpy(str, st.str);
	cout << num_strings << ": \"" << str
		<< "\" object created\n";	// for your information
}

StringBad::~StringBad()
{
	cout << "\"" << str << "\" object deleted, ";		// FYI
	--num_strings;										// required
	cout << num_strings << " left\n";			// FYI
	delete[] str;
}

StringBad & StringBad::operator=(const StringBad & st)
{
	if (this == &st)							// object assigned to itself
		return *this;
	delete[] str;								// free old string
	len = st.len;
	str = new char[len + 1];			// get space for new string
	std::strcpy(str, st.str);				// copy the string

	return *this;								// return reference to invoking object
}

std::ostream & operator<<(std::ostream & os, const StringBad & st)
{
	os << st.str;
	return os;
}
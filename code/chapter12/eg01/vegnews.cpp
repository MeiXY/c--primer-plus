// vegnews.cpp  --  using new and delete with classes
// compile with stringbad.cpp
#include<iostream>
#include "stringbad.h"
using std::cout;
void callme1(StringBad &);   // pass by reference
void callme2(StringBad);       // pass by value

int main()
{
	using std::endl;
	{
		cout << "start." << endl;
		StringBad str1 = StringBad("Celery Stalks at Midnight");
		StringBad str2 = StringBad("Lettuce Prey");
		StringBad str3 = StringBad("Spinach Leaves Bowl for Dollars");
		
		cout << "str1: " << str1 << endl;
		cout << "str2: " << str2 << endl;
		cout << "str3: " << str3 << endl;

		callme1(str1);
		cout << "str1: " << str1 << endl;
		callme2(str2);
		cout << "str2: " << str2 << endl;
		// initialize one object to another
		StringBad str4 = str3;
		cout << "str4: " << str4 << endl;
		// assign one object to another
		StringBad str5;
		str5 = str3;
		cout << "str5: " << str5 << endl;

	}
	cout << "finished!" << endl;

	return 0;
}

void callme1(StringBad & rsb)
{
	cout << "String passed by reference:\n";
	cout << "        \"" << rsb << " \"\n";
}

void callme2(StringBad sb)
{
	cout << "String passed by value:\n";
	cout << "           \"" << sb << "\"\n";
}
// Cow.cpp
#include "Cow.h"
#include<cstring>
#include<iostream>

CCow::CCow()
{
	m_pszHobby = new char[20];
	std::strcpy(m_pszName, "none");
	std::strcpy(m_pszHobby, "none");
	m_dbWeight = 0.0;
}

CCow::CCow(const char * pszNm, const char * pszHo, double dbWt)
{
	m_pszHobby = new char[20];
	std::strcpy(m_pszName, pszNm);	
	std::strcpy(m_pszHobby, pszHo);
	m_dbWeight = dbWt;
}

CCow::CCow(const CCow & c)
{
	m_pszHobby = new char[20];
	std::strcpy(m_pszName, c.m_pszName);
	std::strcpy(m_pszHobby, c.m_pszHobby);
	m_dbWeight = c.m_dbWeight;
}

CCow::~CCow()
{
	delete[]m_pszHobby;
	m_pszHobby = NULL;
}

CCow & CCow::operator=(const CCow & c)
{
	// TODO: insert return statement here	
	m_pszHobby = new char[20];
	std::strcpy(m_pszName, c.m_pszName);
	std::strcpy(m_pszHobby, c.m_pszHobby);
	m_dbWeight = c.m_dbWeight;

	return *this;
}

void CCow::ShowCow() const
{
	std::cout << "name: " << m_pszName << std::endl;
	std::cout << "hobby: " << m_pszHobby << std::endl;
	std::cout << "weight: " << m_dbWeight << std::endl;
}

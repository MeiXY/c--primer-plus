// Cow.h
#pragma once

class CCow
{
private:
	char m_pszName[20];
	char* m_pszHobby;
	double m_dbWeight;

public:
	CCow();
	CCow(const char* pszNm, const char* pszHo, double dbWt);
	CCow(const CCow& c);
	~CCow();

	CCow& operator=(const CCow& c);
	void ShowCow() const;
};

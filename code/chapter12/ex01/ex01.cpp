// ex01.cpp
#include"Cow.h"
#include<iostream>

int main(int argc, char* argv[])
{
	char* pszCowName = new char[20];
	char* pszCowHobby = new char[20];
	double dbWt = 0;

	std::cout << "please input 3 parameters:" << std::endl;
	std::cout << "name: ";
	std::cin >> pszCowName;
	/*std::cin.getline()*/
	std::cout << "hobby: ";
	std::cin >> pszCowHobby;
	std::cout << "weight: ";
	std::cin >> dbWt;

	CCow cowA(pszCowName, pszCowHobby, dbWt);
	CCow cowB;

	cowB = cowA;
	std::cout << "cowA:" << std::endl;
	cowA.ShowCow();
	std::cout << "cowB" << std::endl;
	cowB.ShowCow();

	delete[] pszCowHobby;
	delete[] pszCowName;

	system("pause");
	return 0;
}

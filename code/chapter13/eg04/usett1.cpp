//  usett1.cpp  --  using base class and derived class
#include <iostream>
#include "tabtenn1.h"

int main(void)
{
	using std::cout;
	using std::endl;

	TableTennisPlayer  player1("Smith", "John", false);
	RatedPlayer ratedPlayer1(100, "Lennon", "John", true);

	player1.Name();
	if (player1.HasTable())
		cout << " has a table." << endl;
	else
		cout << " hasn't a table." << endl;
	// reset
	player1.ResetTable(true);
	cout << "after table reseted:" << endl;
	player1.Name();
	if (player1.HasTable())
		cout << " has a table." << endl;
	else
		cout << " hasn't a table." << endl;

	ratedPlayer1.Name();
	if (ratedPlayer1.HasTable())
		cout << " has a table." << endl;
	else
		cout << " hasn't a table." << endl;
	// after table reseted
	ratedPlayer1.ResetTable(false);
	cout << "after table reseted:" << endl;
	ratedPlayer1.Name();
	if (ratedPlayer1.HasTable())
		cout << " has a table." << endl;
	else
		cout << " hasn't a table." << endl;

	ratedPlayer1.Name();
	cout << ", rating: " << ratedPlayer1.Rating() << endl;
	// after rate reseted
	ratedPlayer1.ResetRating(150);
	cout << "after rate reseted:" << endl;
	ratedPlayer1.Name();
	cout << ", rating: " << ratedPlayer1.Rating() << endl;

	RatedPlayer ratedPlayer2(200, player1);

	ratedPlayer2.Name();
	cout << ", rating: " << ratedPlayer2.Rating() << endl;

	return 0;
}
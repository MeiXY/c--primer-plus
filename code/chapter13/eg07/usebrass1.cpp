// usebrass1.cpp  --  testing bank account classes
// compile with brass.cpp
#include <iostream>
#include "brass.h"

int main()
{
	using std::cout;
	using std::endl;

	Brass acont1("Li", 3001, 1000);
	BrassPlus acont2("Liu", 3002, 3000);
	// acont1
	acont1.ViewAcct();
	cout << endl;	
	cout << "Depositing $800 into the acont1 account:\n";
	acont1.Deposit(800);
	cout << "New balance: $" << acont1.Balance() << endl;
	cout << endl;
	cout << "Withdrawing $1000 from the acont1 account:\n";
	acont1.Withdraw(1000);
	cout << "New balance: $" << acont1.Balance() << endl;
	cout << endl;
	acont1.ViewAcct();
	cout << endl;
	// acont2
	acont2.ViewAcct();
	cout << endl;
	cout << "Depositing $500 into the acont2 account:\n";
	acont2.Deposit(500);
	cout << "New balance: $" << acont2.Balance() << endl;
	cout << endl;
	cout << "Withdrawing $4000 from the acont2 account:\n";
	acont2.Withdraw(4000);
	cout << "New balance: $" << acont2.Balance() << endl;
	cout << endl;
	acont2.ViewAcct();
	cout << endl;
}
// usebrass2.cpp  --  polymorphic example
// compile with brass.cpp
#include <iostream>
#include <string>
#include "brass.h"
const int NUM = 4;

int main()
{
	using std::cin;
	using std::cout;
	using std::endl;

	Brass *pAcont[NUM]; // pointer array to class object
	// class object parameters
	std::string tempName;
	long tempNum;
	double tempBal;
	double tempMax;
	double tempRate;
	char dir; // direction to inputing class object

	for (int i = 0; i < NUM; i++)
	{
		cout << "1-Brass, 2-BrassPlus." << endl;		
		while (cin >> dir && (dir != '1'&&dir != '2'))
			cout << "1-Brass, 2-BrassPlus." << endl;
		while (cin.get() != '\n') // important, reduce buffer
			continue;
		cout << "please enter client's name: ";
		getline(cin, tempName);
		cout << "please enter client's account number: ";
		cin >> tempNum;
		cout << "please enter opening balance: $";
		cin >> tempBal;
		if (dir == '1')
		{
			pAcont[i] = new Brass(tempName, tempNum, tempBal);
		}
		else // dir=='2'
		{
			cout << "please enter max loan: $";
			cin >> tempMax;
			cout << "please enter rate: ";
			cin >> tempRate;
			pAcont[i] = new BrassPlus(tempName, tempNum, tempBal, tempMax, tempRate);
		}
		while (cin.get() != '\n') // important, reduce buffer
			continue;
	}// for (int i = 0; i < NUM; i++)

	cout << "\nview account:";
	for (int i = 0; i < NUM; i++)
	{
		pAcont[i]->ViewAcct();
		cout << endl;
	}
	// delete pointer
	for (int i = 0; i < NUM; i++)
	{
		delete pAcont[i];
	}

	return 0;
}
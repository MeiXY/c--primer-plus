// usedma.cpp -- inheritance, friends, and DMA
// compile with dma.cpp
#include<iostream>
#include"dma.h"
int main()
{
	using std::cout;
	using std::endl;

	baseDMA shirt("portabelly", 8);
	lacksDMA balloon("red", "blimpo", 4);
	hasDMA map("mercator", "buffalo keys", 5);
	cout << "displaying baseDMA object:\n";
	cout << shirt << endl;
	cout << "displaying lacksDMA object:\n";
	cout << balloon << endl;
	cout << "displaying hasDMA object:\n";
	cout << map << endl;
	lacksDMA balloon2(balloon);
	cout << "result of lacksDMA copy:\n";
	cout << balloon2 << endl;
	hasDMA map2;
	map2 = map;
	cout << "result of hasDMA copy:\n";
	cout << map2 << endl;
	return 0;
}
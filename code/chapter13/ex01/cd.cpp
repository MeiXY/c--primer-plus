#include "cd.h"
#include<cstring>
#include<iostream>

CCd::CCd() 
{
	std::strcpy(m_pszPerformers, "none");
	std::strcpy(m_pszLabel,"none");
	m_nSelections = 0;
	m_dbPlaytime = 0.0;
}

CCd::CCd(char * pszS1, char*pszS2, int n, double db) 
{
	std::strcpy(m_pszPerformers, pszS1);
	std::strcpy(m_pszLabel, pszS2);
	m_nSelections = n;
	m_dbPlaytime = db;
}

CCd::CCd(const CCd & d) 
{
	std::strcpy(m_pszPerformers, d.m_pszPerformers);
	std::strcpy(m_pszLabel, d.m_pszLabel);
	m_nSelections = d.m_nSelections;
	m_dbPlaytime = d.m_dbPlaytime;
}

void CCd::Report() const 
{
	std::cout << "performers: " << m_pszPerformers << std::endl;
	std::cout << "label: " << m_pszLabel << std::endl;
	std::cout << "selections: " << m_nSelections << std::endl;
	std::cout << "playtime: " << m_dbPlaytime << std::endl;
}

CCd& CCd::operator=(const CCd & d) 
{
	if (this == &d)
		return *this;
	std::strcpy(m_pszPerformers, d.m_pszPerformers);
	std::strcpy(m_pszLabel, d.m_pszLabel);
	m_nSelections = d.m_nSelections;
	m_dbPlaytime = d.m_dbPlaytime;

	return *this;
}

CCd::~CCd()
{
	;
}
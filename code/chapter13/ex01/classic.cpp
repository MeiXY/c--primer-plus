#include"classic.h"
#include<iostream>
#include<cstring>

CClassic::CClassic() 
{
	std::strcpy(m_pszWorks, "none");
}

CClassic::CClassic(char * s, char * s1, char*s2, int n, double x) :CCd(s1, s2, n, x) 
{
	std::strcpy(m_pszWorks, s);
}

CClassic::CClassic(char * s, const CCd & d) : CCd(d) 
{
	std::strcpy(m_pszWorks, s);
}

CClassic::CClassic(const CClassic & c) : CCd(c) 
{
	std::strcpy(m_pszWorks, c.m_pszWorks);
}

CClassic::~CClassic()
{
	;
}

void CClassic::Report() const 
{
	std::cout << "works: " << m_pszWorks << std::endl;
	CCd::Report();
}

CClassic & CClassic::operator=(const CClassic & c)
{
	if (this == &c)
		return *this;
	CCd::operator=(c);
	strcpy(m_pszWorks, c.m_pszWorks);

	return *this;
}

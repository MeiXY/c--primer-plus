#ifndef CLASSIC_H_
#define CLASSIC_H_
#include "cd.h"
class CClassic :public CCd
{
private:	
	char m_pszWorks[50];
public:
	CClassic();
	CClassic(char * psz, char * pszS1, char* pszS2, int n, double db);
	CClassic(char * psz, const CCd & d);
	CClassic(const CClassic & c);
	virtual ~CClassic();
	void Report() const;
	CClassic & operator=(const CClassic & c);
};
#endif

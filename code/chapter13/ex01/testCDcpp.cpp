#include<iostream>
using namespace std;
#include "classic.h"

void Bravo(const CCd & cdDisk);

int main()
{
	CCd cd1("Beatles", "Capitol", 14, 35.5);
	CClassic cd2 = CClassic("Piano Sonata in B flat, Fantasia in C", "Alfred Brendel", "Philips", 2, 57.17);
	CCd * pcd = &cd1;

	cout << "using object directly:\n";
	cd1.Report();	// use Cd method
	cd2.Report();	// use Classic method

	cout << "using type cd * pointer to objects:\n";
	pcd->Report();	// use Cd method for cd object
	pcd = &cd2;
	pcd->Report();	// use Clssic method for classic object

	cout << "calling a function with a Cd reference argument:\n";
	Bravo(cd1);
	Bravo(cd2);

	cout << "Testing assignment: ";
	CClassic copy;
	copy = cd2;
	copy.Report();

	system("pause");
	return 0;
}

void Bravo(const CCd & disk)
{
	disk.Report();
}
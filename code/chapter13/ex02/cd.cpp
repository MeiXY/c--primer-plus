#include "cd.h"
#include<string.h>
#include<iostream>
#include<assert.h>

CCd::CCd(): m_pszPerformers(NULL), m_pszLabel(NULL)
{
	if (NULL != m_pszPerformers)
	{
		delete[] m_pszPerformers;
		m_pszPerformers = NULL;
	}

	m_pszPerformers = new char[5];
	assert(NULL != m_pszPerformers);
	memset(m_pszPerformers, 0, sizeof(m_pszPerformers));

	if (NULL != m_pszLabel)
	{
		delete[] m_pszLabel;
		m_pszLabel = NULL;
	}

	m_pszLabel = new char[5];
	assert(NULL != m_pszLabel);
	memset(m_pszLabel, 0, sizeof(m_pszLabel));

	strcpy(m_pszPerformers, "none");
	strcpy(m_pszLabel,"none");
	m_nSelections = 0;
	m_dbPlaytime = 0.0;
}

CCd::CCd(char * pszS1, char*pszS2, int n, double db): m_pszPerformers(NULL), m_pszLabel(NULL)
{
	if (NULL != m_pszPerformers)
	{
		delete[] m_pszPerformers;
		m_pszPerformers = NULL;
	}

	int nStrLen = NULL == pszS1 ? 4 : strlen(pszS1);
	m_pszPerformers = new char[nStrLen + 1];
	assert(NULL != m_pszPerformers);
	memset(m_pszPerformers, 0, sizeof(m_pszPerformers));

	if (NULL != m_pszLabel)
	{
		delete[] m_pszLabel;
		m_pszLabel = NULL;
	}

	nStrLen = NULL == pszS2 ? 4 : strlen(pszS2);
	m_pszLabel = new char[nStrLen + 1];
	assert(NULL != m_pszLabel);
	memset(m_pszLabel, 0, sizeof(m_pszLabel));

	NULL == pszS1 ? strcpy(m_pszPerformers, "none") : strcpy(m_pszPerformers, pszS1);
	NULL == pszS2 ? strcpy(m_pszLabel, "none") : strcpy(m_pszLabel, pszS2);
	m_nSelections = n;
	m_dbPlaytime = db;
}

CCd::CCd(const CCd & d) 
{
	if (NULL != m_pszPerformers)
	{
		delete[] m_pszPerformers;
		m_pszPerformers = NULL;
	}

	int nStrLen = NULL == d.m_pszPerformers ? 4 : strlen(d.m_pszPerformers);
	m_pszPerformers = new char[nStrLen + 1];
	assert(NULL != m_pszPerformers);
	memset(m_pszPerformers, 0, sizeof(m_pszPerformers));

	if (NULL != m_pszLabel)
	{
		delete[] m_pszLabel;
		m_pszLabel = NULL;
	}

	nStrLen = NULL == d.m_pszLabel ? 4 : strlen(d.m_pszLabel);
	m_pszLabel = new char[nStrLen + 1];
	assert(NULL != m_pszLabel);
	memset(m_pszLabel, 0, sizeof(m_pszLabel));

	NULL == d.m_pszPerformers ? strcpy(m_pszPerformers, "none") : strcpy(m_pszPerformers, d.m_pszPerformers);
	NULL == d.m_pszLabel ? strcpy(m_pszLabel, "none") : strcpy(m_pszLabel, d.m_pszLabel);
	m_nSelections = d.m_nSelections;
	m_dbPlaytime = d.m_dbPlaytime;
}

void CCd::Report() const 
{
	std::cout << "performers: " << m_pszPerformers << std::endl;
	std::cout << "label: " << m_pszLabel << std::endl;
	std::cout << "selections: " << m_nSelections << std::endl;
	std::cout << "playtime: " << m_dbPlaytime << std::endl;
}

CCd& CCd::operator=(const CCd & d) 
{
	if (this == &d)
	{
		return *this;
	}

	if (NULL != m_pszPerformers)
	{
		delete[] m_pszPerformers;
		m_pszPerformers = NULL;
	}

	int nStrLen = NULL == d.m_pszPerformers ? 4 : strlen(d.m_pszPerformers);
	m_pszPerformers = new char[nStrLen + 1];
	assert(NULL != m_pszPerformers);
	memset(m_pszPerformers, 0, sizeof(m_pszPerformers));

	if (NULL != m_pszLabel)
	{
		delete[] m_pszLabel;
		m_pszLabel = NULL;
	}

	nStrLen = NULL == d.m_pszLabel ? 4 : strlen(d.m_pszLabel);
	m_pszLabel = new char[nStrLen + 1];
	assert(NULL != m_pszLabel);
	memset(m_pszLabel, 0, sizeof(m_pszLabel));

	NULL == d.m_pszPerformers ? strcpy(m_pszPerformers, "none") : strcpy(m_pszPerformers, d.m_pszPerformers);
	NULL == d.m_pszLabel ? strcpy(m_pszLabel, "none") : strcpy(m_pszLabel, d.m_pszLabel);
	m_nSelections = d.m_nSelections;
	m_dbPlaytime = d.m_dbPlaytime;

	return *this;
}

CCd::~CCd()
{
	if (NULL != m_pszPerformers)
	{
		delete[] m_pszPerformers;
		m_pszPerformers = NULL;
	}

	if (NULL != m_pszLabel)
	{
		delete[] m_pszLabel;
		m_pszLabel = NULL;
	}
}
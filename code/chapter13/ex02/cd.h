#ifndef CD_H_
#define CD_H_
// base class
class CCd {// represents a CD disk
private:
	char* m_pszPerformers;
	char* m_pszLabel;
	int m_nSelections;	// number of selections
	double m_dbPlaytime;	// playing time in minutes
public:
	CCd(char * s1, char*s2, int n, double x);
	CCd(const CCd & d);
	CCd();
	virtual ~CCd();
	virtual void Report() const;	// reports all CD data
	CCd & operator=(const CCd & d);
};
#endif // !CD_H_

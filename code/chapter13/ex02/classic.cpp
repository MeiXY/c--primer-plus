#include"classic.h"
#include<iostream>
#include<string.h>
#include<assert.h>

CClassic::CClassic(): m_pszWorks(NULL)
{
	if (NULL != m_pszWorks)
	{
		delete[] m_pszWorks;
		m_pszWorks = NULL;
	}

	m_pszWorks = new char[5];
	assert(NULL != m_pszWorks);
	memset(m_pszWorks, 0, sizeof(m_pszWorks));

	strcpy(m_pszWorks, "none");
}

CClassic::CClassic(char * s, char * s1, char*s2, int n, double x) :CCd(s1, s2, n, x), m_pszWorks(NULL)
{
	if (NULL != m_pszWorks)
	{
		delete[] m_pszWorks;
		m_pszWorks = NULL;
	}

	int nStrLen = NULL == s ? 4 : strlen(s);
	m_pszWorks = new char[nStrLen + 1];
	assert(NULL != m_pszWorks);
	memset(m_pszWorks, 0, sizeof(m_pszWorks));

	NULL == s ? strcpy(m_pszWorks, "none") : strcpy(m_pszWorks, s);
}

CClassic::CClassic(char * s, const CCd & d) : CCd(d), m_pszWorks(NULL)
{
	if (NULL != m_pszWorks)
	{
		delete[] m_pszWorks;
		m_pszWorks = NULL;
	}

	int nStrLen = NULL == s ? 4 : strlen(s);
	m_pszWorks = new char[nStrLen + 1];
	assert(NULL != m_pszWorks);
	memset(m_pszWorks, 0, sizeof(m_pszWorks));

	NULL == s ? strcpy(m_pszWorks, "none") : strcpy(m_pszWorks, s);
}

CClassic::CClassic(const CClassic & c) : CCd(c), m_pszWorks(NULL)
{
	if (NULL != m_pszWorks)
	{
		delete[] m_pszWorks;
		m_pszWorks = NULL;
	}

	int nStrLen = NULL == c.m_pszWorks ? 4 : strlen(c.m_pszWorks);
	m_pszWorks = new char[nStrLen + 1];
	assert(NULL != m_pszWorks);
	memset(m_pszWorks, 0, sizeof(m_pszWorks));

	NULL == c.m_pszWorks ? strcpy(m_pszWorks, "none") : strcpy(m_pszWorks, c.m_pszWorks);
}

CClassic::~CClassic()
{
	if (NULL != m_pszWorks)
	{
		delete[] m_pszWorks;
		m_pszWorks = NULL;
	}
}

void CClassic::Report() const 
{
	std::cout << "works: " << m_pszWorks << std::endl;
	CCd::Report();
}

CClassic & CClassic::operator=(const CClassic & c)
{
	if (this == &c)
	{
		return *this;
	}
	CCd::operator=(c);

	if (NULL != m_pszWorks)
	{
		delete[] m_pszWorks;
		m_pszWorks = NULL;
	}

	int nStrLen = NULL == c.m_pszWorks ? 4 : strlen(c.m_pszWorks);
	m_pszWorks = new char[nStrLen + 1];
	assert(NULL != m_pszWorks);
	memset(m_pszWorks, 0, sizeof(m_pszWorks));

	NULL == c.m_pszWorks ? strcpy(m_pszWorks, "none") : strcpy(m_pszWorks, c.m_pszWorks);

	return *this;
}
